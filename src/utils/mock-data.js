const faker = require("faker");

 module.exports.getMockTruck = () => ({
    code: `CODE_${faker.unique(() => faker.datatype.number(10000))}`,
    name: faker.name.title(),
    status: "OUT_OF_SERVICE",
    description: faker.lorem.sentence(),
});