const { spec } = require('pactum');

const apiUrl = 'http://qa-api-mock-3.eu-central-1.elasticbeanstalk.com/trucks';

module.exports = {
  postTruck: async function (truckData, expectedStatus = 201) {
    const response = await spec()
      .post(apiUrl)
      .withJson(truckData)
      .expectStatus(expectedStatus);
  
    if (expectedStatus === 201) {
      const responseBody = response.json;
      return responseBody;
    }
  },
  putTrack: async function(truckData, truckId, expectedStatus = 200){
    const response = await spec()
    .put(`${apiUrl}/${truckId}`)
    .withBody(truckData)
    .expectStatus(expectedStatus)

    const responseBody = response.json;
    return responseBody;
  },
  apiUrl: apiUrl
};
