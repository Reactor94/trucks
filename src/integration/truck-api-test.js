const { spec } = require('pactum');
const assert = require('assert');
const faker = require("faker");
const { getMockTruck } = require('../utils/mock-data');
const { postTruck, apiUrl, putTrack } = require('../utils/api');

describe('API Tests', () => {

  let postTruckCreate
  const expectedProperties = ["code", "name", "status", "description", "id"];

  beforeEach(() => {
    postTruckCreate = getMockTruck()
  })

  it('should create truck', async () => {
    const responseBody = await postTruck(postTruckCreate, 201)
    expectedProperties.forEach((property) => {
      assert(property in responseBody, `${property} should exist in the response`);
      assert(responseBody[property], `${property} should have a non-empty value`);
    });
  });

  it('should create truck without description', async () => {
    const postTruckCreateWithoutDescription = {
      code: `CODE_${Math.floor(Math.random() * 10000)}`,
      name: faker.name.title(),
      status: "OUT_OF_SERVICE",
    };

    const responseBody = await postTruck(postTruckCreateWithoutDescription, 201)
    expectedProperties.filter(property => property !== "description").forEach(property => {
      assert(property in responseBody, `${property} should exist in the response`)
      assert(responseBody[property], `${property} should have a non-empty value`);
    })
  });

  it('should not create truck without Code field', async () => {
    await postTruck({ name: faker.name.title(), status: "OUT_OF_SERVICE" }, 400)
  });

  it('should not create truck without Name field', async () => {
    await postTruck({ code: `CODE_${faker.datatype.number(10000)}`, status: "OUT_OF_SERVICE" }, 400)
  });

  it('should not create truck without Status field', async () => {
    await postTruck({ code: `CODE_${faker.datatype.number(10000)}`, name: faker.name.title() }, 400)
  });

  it('should get the created truck', async () => {
    const postBody = await postTruck(postTruckCreate, 201)

    if (!postBody) {
      throw new Error('No truck data available for testing');
    }

    const response = await spec()
      .get(`${apiUrl}/${postBody.id}`)
      .expectStatus(200);

    const getBody = response.json;

    assert.strictEqual(getBody.id, postBody.id, 'Truck ID should match the created truck ID');
    assert.strictEqual(getBody.name, postBody.name, 'Truck name should match the created truck name');
    assert.strictEqual(getBody.status, postBody.status, 'Truck status should match the created truck status');
    assert.strictEqual(getBody.code, postBody.code, 'Truck code should match the created truck code');
  });

  it('should update created truck', async () => {
    const postBody = await postTruck(postTruckCreate, 201)
    const updatedTruck = {
      code: `CODE_${faker.unique(() => faker.datatype.number(10000))}`,
      name: "New name",
      status: "LOADING",
      description: "New description"
    }

    if (!postBody) {
      throw new Error('No truck data available for testing');
    }

    const response = await putTrack(updatedTruck, postBody.id)
    expectedProperties.filter(property => property !== "id").forEach(property => {
      assert.strictEqual(response[property], updatedTruck[property], `${property} was not changed`)
    })
  });

  it('should return 400 Bad Request when updating truck status directly to "At Job" from "LOADING"', async () => {
    const postBody = await postTruck({ ...postTruckCreate, status: 'LOADING' }, 201)

    if (!postBody) {
      throw new Error('No truck data available for testing');
    }
    const updatedStatus = 'AT_JOB';
    await putTrack({ status: updatedStatus }, postBody.id, 400)
  });

  it('should update truck status in the proper order', async () => {
    const postBody = await postTruck(postTruckCreate, 201)

    if (!postBody) {
      throw new Error('No truck data available for testing');
    }

    const statusOrder = ["TO_JOB", "AT_JOB", "RETURNING", "LOADING"];

      for (const newStatus of statusOrder) {
        const updateTruck = {
          status: newStatus,
        };


      const response = await putTrack(updateTruck, postBody.id)
      assert.strictEqual(response.status, newStatus, `Truck STATUS should match the status "${newStatus}"`);
    }
  })

  it('should get the created truck by status', async () => {
    const postBody1 = await postTruck(postTruckCreate, 201)

    if (!postBody1) {
      throw new Error('No truck data available for testing');
    }

    const response = await spec()
      .get(`${apiUrl}?status=OUT_OF_SERVICE`)
      .expectStatus(200);

    const responseBody = response.json;
    responseBody.forEach(truck => assert(truck.status === 'OUT_OF_SERVICE'))
  });

  it('should get the created trucks by status with limit and sorting', async () => {
    const postBody1 = await postTruck(postTruckCreate, 201);
    const postBody2 = await postTruck({ ...postTruckCreate, status: 'LOADING', code: `CODE_${faker.unique(() => faker.datatype.number(10000))}` }, 201);
    const postBody3 = await postTruck({ ...postTruckCreate, status: 'TO_JOB', code: `CODE_${faker.unique(() => faker.datatype.number(10000))}` }, 201);

    if (!postBody1 || !postBody2 || !postBody3) {
      throw new Error('No truck data available for testing');
    }

    const response = await spec()
      .get(`${apiUrl}?status=OUT_OF_SERVICE&limit=100&sort=status&order=asc`)
      .expectStatus(200);

    const responseBody = response.json;

    const sortedByStatus = responseBody.map(truck => truck.status);
    const isSorted = [...sortedByStatus].sort().toString() === sortedByStatus.toString();

    assert(responseBody.length <= 100, 'The number of results should be less than or equal to the specified limit');
    assert(isSorted, 'Truck statuses should be sorted in ascending order');
  });

  it('should get the created trucks sorted by id', async () => {
    const postBody1 = await postTruck(postTruckCreate, 201);
    const postBody2 = await postTruck({ ...postTruckCreate, status: 'LOADING', code: `CODE_${faker.unique(() => faker.datatype.number(10000))}` }, 201);
    const postBody3 = await postTruck({ ...postTruckCreate, status: 'TO_JOB', code: `CODE_${faker.unique(() => faker.datatype.number(10000))}` }, 201);

    if (!postBody1 || !postBody2 || !postBody3) {
      throw new Error('No truck data available for testing');
    }

    const response = await spec()
      .get(`${apiUrl}?limit=100&sort=id&page=1`)
      .expectStatus(200);

    const responseBody = response.json;

    const truckIds = responseBody.map(truck => truck.id);
    const isSorted = truckIds.every((id, index, array) => {
      if (index === 0) return true;
      return id >= array[index - 1];
    });
    assert(isSorted, 'Truck IDs should be sorted in ascending order');
  });
});

