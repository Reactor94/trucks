## Truck API Tests

These tests are designed to verify the behavior of the Truck API, specifically for creating, retrieving, and updating truck records. The tests cover both positive and negative scenarios to ensure the API functions as expected.

## Prerequisites
Before running the tests, you'll need the following:

Node.js installed on your machine.
The pactum package for API testing. You can install it using: **npm install pactum**

The faker package for generating fake data. You can install it using **npm install faker**

Mocha for running the tests. You can install it using **npm install mocha**.

## Running the Tests
To run the tests, use the following command: **npm run test**

- no tests regarding max string length because there no restrictions
- no test for get endpoint which check that we allow searching via contains logic or that the inputs are case sensative
- PUT allows sending empty requests which empty out truck objects which should not be possible, we should either ignore such requests or we should get an error that mandatiry fields are missing
